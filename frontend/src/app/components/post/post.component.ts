import { Component, Input, OnDestroy, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { Post } from 'src/app/models/post/post';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { LikeService } from 'src/app/services/like.service';
import { NewComment } from 'src/app/models/comment/new-comment';
import { CommentService } from 'src/app/services/comment.service';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { MainThreadComponent } from '../main-thread/main-thread.component';
import { ImgurService } from 'src/app/services/imgur.service';
import { Image } from 'src/app/models/image';
import { Reaction } from 'src/app/models/reactions/reaction';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;
    public newImage: File;
    public isUpdating: boolean = false;
    private unsubscribe$ = new Subject<void>();
    public constructor(
        private mainThreadComponent: MainThreadComponent,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private imgurService: ImgurService,
        private toastrService: ToastrService
    ) { }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public getReactionsCount(isPositive: boolean) {
        return this.post.reactions.filter(x => x.isLike == isPositive).length;
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }
    public getLikedUsers() {
        let usersList: string[] = new Array();
        let reactions = this.post.reactions.filter(x => x.isLike);
        reactions.forEach(r => usersList.push(r.user.userName + '\n'));
        return usersList;
    }

    public getDislikedUsers() {
        let usersList: string[] = new Array();
        let reactions = this.post.reactions.filter(x => !x.isLike);
        reactions.forEach(r => usersList.push(r.user.userName + '\n'));
        return usersList;
    }

    public toggleUpdating() {
        this.isUpdating = !this.isUpdating;
    }
    public checkAuthor() {
        if (this.currentUser)
            return this.post.author.id === this.currentUser.id;
    }
    public deletePost() {
        this.postService.deletePost(this.post.id).subscribe(() => {
            this.mainThreadComponent.getPosts();
            this.toastrService.success("Post was deleted succesfully", 'Success');
        });

    }

    public editPost(newBody: string) {
        this.post.body = newBody;
        if (this.newImage) {
            this.imgurService.uploadToImgur(this.newImage, 'title').pipe(
                switchMap((imageData) => (this.post.previewImage = imageData.body.data.link)));
        }

        this.postService.updatePost(this.post)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((data) => {
                this.post = data.body;
            });
        this.isUpdating = !this.isUpdating;
        this.toastrService.success("Post was updated succesfully", 'Success')
    }

    public changeImage(target: any) {
        this.newImage = target.files[0];

        if (!this.newImage) {
            target.value = '';
            return;
        }

        if (this.newImage.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
        reader.readAsDataURL(this.newImage);
    }

    public removeImage() {
        this.post.previewImage = '';
    }

    public likePost(isPositive: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, isPositive)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;
                });

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, isPositive)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
