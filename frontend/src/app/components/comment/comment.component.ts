import { Component, Input, OnDestroy } from '@angular/core';
import { Comment } from 'src/app/models/comment/comment';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';
import { Observable, empty, Subject } from 'rxjs';
import { MainThreadComponent } from '../main-thread/main-thread.component';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    private unsubscribe = new Subject<void>();
    public isUpdating = false;

    public constructor(
        private authService: AuthenticationService,
        private snackBarService: SnackBarService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private mainThreadComponent: MainThreadComponent,
        private toastrService: ToastrService
    ) { }
    public ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    public getReactionsCount(isPositive: boolean) {
        return this.comment.reactions.filter(x => x.isLike == isPositive).length;
    }
    public getLikedUsers() {
        let usersList: string[] = new Array();
        let reactions = this.comment.reactions.filter(x => x.isLike);
        reactions.forEach(r => usersList.push(r.user.userName + '\n'));
        return usersList;
    }
    public toggleUpdating() {
        this.isUpdating = !this.isUpdating;
    }
    public getDislikedUsers() {
        let usersList: string[] = new Array();
        let reactions = this.comment.reactions.filter(x => !x.isLike);
        reactions.forEach(r => usersList.push(r.user.userName + '\n'));
        return usersList;
    }
    public editComment(newCommentBody: string) {
        this.comment.body = newCommentBody;
        this.commentService.updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((response) => this.comment = response.body),

            (error) => this.snackBarService.showErrorMessage(error);

        this.toggleUpdating()
        this.toastrService.success("Comment was updated succesfully", "Success");
    }

    public checkAuthor() {
        if (this.currentUser && this.comment) {
            return this.comment.author.id === this.currentUser.id;
        }
    }
    public deleteComment() {
        this.commentService.deleteComment(this.comment.id).subscribe(() => {
            this.toastrService.success("Comment was deleted succesfully", "Success");
            this.mainThreadComponent.getPosts()
        });
    }
    public likeComment(isPositive: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, isPositive)),
                    takeUntil(this.unsubscribe)
                )
                .subscribe((post) => {
                    this.comment = this.comment;
                    // this.mainThreadComponent.getPosts();
                });

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, isPositive)
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((post) => {
                this.comment = this.comment;
                // this.mainThreadComponent.getPosts();
            });
    }
    unsubscribe$(unsubscribe$: any): import("rxjs").OperatorFunction<Comment, Comment> {
        throw new Error("Method not implemented.");
    }
    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
    openAuthDialog() {
        throw new Error("Method not implemented.");
    }

}
