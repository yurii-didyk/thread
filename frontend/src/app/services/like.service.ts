import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './comment.service';
import { Comment } from '../models/comment/comment';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    public likePost(post: Post, currentUser: User, isPositive: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isPositive,
            userId: currentUser.id
        };
        let hasLikes = innerPost.reactions.some(x => x.user.id === currentUser.id && x.isLike);
        let hasDislikes = innerPost.reactions.some(x => x.user.id === currentUser.id && !x.isLike);
        if (hasLikes && reaction.isLike || hasDislikes && !reaction.isLike) {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
        }
        else if (!hasLikes && !hasDislikes) {
            innerPost.reactions = innerPost.reactions.concat({ isLike: isPositive, user: currentUser })
        }
        else {
            innerPost.reactions = innerPost.reactions.filter(x => x.user.id !== currentUser.id);
            innerPost.reactions = innerPost.reactions.concat({ isLike: isPositive, user: currentUser });
        }
        hasLikes = innerPost.reactions.some(x => x.user.id === currentUser.id && x.isLike);
        hasDislikes = innerPost.reactions.some(x => x.user.id === currentUser.id && !x.isLike);
        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                if (hasLikes && reaction.isLike || hasDislikes && !reaction.isLike) {
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                if (!hasLikes && !hasDislikes) {
                    innerPost.reactions = innerPost.reactions.concat({ isLike: isPositive, user: currentUser })
                }
                else {
                    innerPost.reactions = innerPost.reactions.filter(x => x.user.id !== currentUser.id);
                    innerPost.reactions = innerPost.reactions.concat({ isLike: isPositive, user: currentUser });
                }

                return of(innerPost);
            })
        );
    }
    public likeComment(comment: Comment, currentUser: User, isPositive: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: isPositive,
            userId: currentUser.id
        };

        let hasLikes = innerComment.reactions.some(x => x.user.id === currentUser.id && x.isLike);
        let hasDislikes = innerComment.reactions.some(x => x.user.id === currentUser.id && !x.isLike);
        if (hasLikes && reaction.isLike || hasDislikes && !reaction.isLike) {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
        }
        else if (!hasLikes && !hasDislikes) {
            innerComment.reactions = innerComment.reactions.concat({ isLike: isPositive, user: currentUser })
        }
        else {
            innerComment.reactions = innerComment.reactions.filter(x => x.user.id !== currentUser.id);
            innerComment.reactions = innerComment.reactions.concat({ isLike: isPositive, user: currentUser });
        }
        hasLikes = innerComment.reactions.some(x => x.user.id === currentUser.id && x.isLike);
        hasDislikes = innerComment.reactions.some(x => x.user.id === currentUser.id && !x.isLike);
        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                if (hasLikes && reaction.isLike || hasDislikes && !reaction.isLike) {
                    innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                else if (!hasLikes && !hasDislikes) {
                    innerComment.reactions = innerComment.reactions.concat({ isLike: isPositive, user: currentUser })
                }
                else {
                    innerComment.reactions = innerComment.reactions.filter(x => x.user.id !== currentUser.id);
                    innerComment.reactions = innerComment.reactions.concat({ isLike: isPositive, user: currentUser });
                }

                return of(innerComment);
            })
        );

    }
}
