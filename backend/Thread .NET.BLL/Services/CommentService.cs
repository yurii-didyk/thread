﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using System.Linq;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task<CommentDTO> UpdateComment(CommentDTO commentDto)
        {
            var commentToUpdate = _context.Comments.First(c => c.Id == commentDto.Id);
            var post = _mapper.Map<Comment>(commentDto);
            if (commentToUpdate != null)
            {
                commentToUpdate.Body= post.Body;
               
                _context.Comments.Update(commentToUpdate);
            }
            await _context.SaveChangesAsync();
            var editedComment = await _context.Comments
               .Include(c => c.Author)
                   .ThenInclude(author => author.Avatar)
               .FirstAsync(c => c.Id == post.Id);
            var editedCommentDto = _mapper.Map<CommentDTO>(editedComment);
           
            return editedCommentDto;
        }
        public async Task DeleteComment(int commentId)
        {
            var comment = _context.Comments.First(c => c.Id == commentId);
            var isCommentReactions = _context.CommentReactions.Any(reaction => reaction.CommentId == commentId);
            var commentsReactions = _context.CommentReactions.Where(c => c.CommentId == commentId);
            if (isCommentReactions)
                _context.CommentReactions.RemoveRange(commentsReactions);
            if (comment != null)
                _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
        }
    }
}
