﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }
        public async Task<PostDTO> UpdatePost(PostDTO postDto)
        {
            var postToUpdate = _context.Posts.First(p => p.Id == postDto.Id);
            var post = _mapper.Map<Post>(postDto);
            if (postToUpdate != null)
            {
                postToUpdate.Body = post.Body;
                postToUpdate.Preview = post.Preview;
                postToUpdate.PreviewId = post.PreviewId;
                _context.Posts.Update(postToUpdate);
            }
            await _context.SaveChangesAsync();
            var editedPost = await _context.Posts
               .Include(p => p.Author)
                   .ThenInclude(author => author.Avatar)
               .Include(p => p.Preview)
               .Include(p => p.Comments)
                   .ThenInclude(comment => comment.Author)
               .FirstAsync(p => p.Id == post.Id);
            var editedPostDto = _mapper.Map<PostDTO>(editedPost);
            await _postHub.Clients.All.SendAsync("UpdatePost", editedPostDto);
            return editedPostDto;
        }

        public async Task DeletePost(int postId)
        {
            var post =  _context.Posts.First(p => p.Id == postId);
            var isComments = _context.Comments.Any(comment => comment.PostId == postId);
            var isPostReactions = _context.PostReactions.Any(reaction => reaction.PostId == postId);
            var comments = _context.Comments.Where(c => c.PostId == postId);
            var postReactions = _context.PostReactions.Where(p => p.PostId == postId);
            foreach (var item in comments)
            {
                _context.CommentReactions.RemoveRange(_context.CommentReactions.Where(c => c.CommentId == item.Id));
            }
            if (isComments)
                _context.Comments.RemoveRange(comments);
            if (isPostReactions)
                _context.PostReactions.RemoveRange(postReactions);
            if (post != null)
                _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }
    }
}
